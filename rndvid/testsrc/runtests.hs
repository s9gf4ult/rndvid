{-# LANGUAGE
  ScopedTypeVariables
  #-}

module Main where

import Blaze.ByteString.Builder
import Blaze.ByteString.Builder.ByteString
import Data.Aeson
import Data.HashMap.Strict
import Data.Text
import Rndvid
import Test.Framework
import Test.Framework.Providers.QuickCheck2
import Test.QuickCheck
import Test.QuickCheck.Assertions
import Test.QuickCheck.Instances

import qualified Data.ByteString.Lazy as B
import qualified Test.QuickCheck.Monadic as M
import qualified Test.QuickCheck.Property as P


-- test1 a = -- M.monadicIO $ do
  -- c1 <- M.run $ do
  --   saveConf a
  --   getConf
  -- M.stop $ a ?== c1

-- test2 a = M.monadicIO $ do
--   (c1, c2) <- M.run $ do
--     saveConf a
--     c1 <- getConf
--     saveConf c1
--     c2 <- getConf
--     return (c1, c2)
--   M.stop $ a ?== c1 .&&. c1 ?== c2


-- main = defaultMain [ testProperty "HashObj" $ \(ar :: [(Text, Integer)]) -> (test-- 1 $ fromList ar)
                   -- , testProperty "HashObj2" $ \(ar :: [(Text, Integer)]) -> (test2 $ fromList ar)
                   -- ]

main :: IO ()
main = error "Not implemented: main"
