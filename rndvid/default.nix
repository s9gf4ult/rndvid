{ mkDerivation, aeson, base, bytestring, containers, directory
, filepath, FilePather, inflections, lens, lib, mime, mime-types
, MonadRandom, mtl, process, random, random-shuffle, text
, unordered-containers, yaml
}:
mkDerivation {
  pname = "rndvid";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = true;
  isExecutable = true;
  libraryHaskellDepends = [
    aeson base bytestring containers directory filepath FilePather
    inflections lens mime mime-types MonadRandom mtl process random
    random-shuffle text unordered-containers yaml
  ];
  executableHaskellDepends = [ base ];
  license = "unknown";
  mainProgram = "rndvid";
}
