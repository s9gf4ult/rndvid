module Rndvid.Main where

import Rndvid.Conf
import Rndvid.DB
import Rndvid.Video
import System.Environment

import qualified Data.HashMap.Strict as H

rndvidMain :: IO ()
rndvidMain = do
  args <- getArgs
  case args of
    ["watched"] -> allWatched
    _ -> watchAll

allWatched :: IO ()
allWatched = do
  vids <- getVideos
  saveDB $ H.fromList $ zip vids $ repeat 1

watchAll :: IO ()
watchAll = do
  vids <- getVideos
  db <- getDB
  let res = mergeVids db vids
  conf <- getConf
  done <- runPlay conf res
  saveDB $ H.filter (>0) $ H.union done db
