module Rndvid.TH where

import           Data.Aeson.TH
import           Data.Char
import           Data.List        as L
import           Data.Text        as T
import           Text.Inflections

jsonOptions :: Options
jsonOptions = defaultOptions
  { fieldLabelModifier = \s -> T.unpack
    $ either (error . show) id
    $ toUnderscore
    $ T.pack
    $ L.dropWhile (not . isUpper) s
  }
