module Rndvid.DB where

import Control.Exception
import Data.Yaml
import System.Directory
import System.FilePath

import qualified Data.HashMap.Strict as H
import qualified Data.Text as T


type DBFiles = H.HashMap T.Text Integer

getDBName :: IO FilePath
getDBName = do
  root <- getXdgDirectory XdgConfig "rndvid"
  createDirectoryIfMissing True root
  return $ root </> "db.yaml"

saveDB :: DBFiles -> IO ()
saveDB files = do
  db <- getDBName
  encodeFile db files

getDB :: IO DBFiles
getDB = do
  db <- getDBName
  doesFileExist db >>= \case
    False -> return H.empty
    True -> do
      decodeFileEither db >>= either throwIO return
