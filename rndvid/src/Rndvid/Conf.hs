module Rndvid.Conf where

import Control.Exception
import Control.Lens
import Data.Aeson.TH
import Data.Map.Strict as M
import Data.Text (Text)
import Data.Yaml
import Rndvid.TH
import System.Directory
import System.FilePath

data Conf = Conf
  { _cDumpDirs :: Map Text Text
  , _cPlayer   :: Maybe Text
  } deriving (Show, Eq)

makeLenses ''Conf
deriveJSON jsonOptions ''Conf

getConfName :: IO String
getConfName = do
  root <- getXdgDirectory XdgConfig "rndvid"
  createDirectoryIfMissing True root
  let result = root </> "conf.yaml"
  doesFileExist result >>= \case
    False -> do
      fail $ "Create config at " ++ result
    True -> return ()
  return result

getConf :: IO Conf
getConf = do
  name <- getConfName
  decodeFileEither name >>= either throwIO return
