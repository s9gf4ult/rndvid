module Rndvid.Video
  ( runPlay
  , getVideos
  , mergeVids
  ) where

import Codec.MIME.Parse
import Codec.MIME.Type
import Control.Lens
import Control.Monad.Random
import Control.Monad.State.Strict
import Data.Char
import Data.Function
import Data.List as L
import Data.Map.Strict as M
import Data.Maybe
import Data.Text (Text)
import Network.Mime
import Rndvid.Conf
import Rndvid.DB
import System.Directory
import System.FilePath
import System.FilePath.FilePather.FileType (FileType(..))
import System.FilePath.FilePather.FilterPredicate (filterPredicate)
import System.FilePath.FilePather.Find
import System.FilePath.FilePather.RecursePredicate (recursePredicate)
import System.Process (rawSystem)
import System.Random.Shuffle (shuffleM)

import qualified Data.HashMap.Strict as H
import qualified Data.Text as T
import qualified Data.Text.Encoding as T

getVideos :: IO [T.Text]
getVideos = do
  putStrLn "Getting videos list"
  pats <- findHere (filterPredicate isVideo) (recursePredicate $ const True)
  mapM (canonicalizePath >=> (return . T.pack)) pats
  where
    isVideo fp File =
      case parseMIMEType $ T.decodeUtf8 $ defaultMimeLookup $ T.pack fp of
        Nothing -> False
        Just a -> case mimeType a of
          Video _ -> True
          _ -> False
    isVideo _ _ = False

-- ^ create new hashmap from list of filenames and replace integer
-- where it exists in conf
mergeVids :: DBFiles -> [Text] -> DBFiles
mergeVids hm files =
  let
    r = H.fromList $ zip files $ repeat 0
    ex = H.intersection hm r
    ret = H.union ex r
  in ret

runPlay :: Conf -> DBFiles -> IO (DBFiles)
runPlay conf db = do
  files <- shuffleMap db
  putStrLn "Start playing"
  ret <- execStateT (contPlay files) db
  return ret
  where
    contPlay :: [T.Text] -> StateT DBFiles IO ()
    contPlay [] = lift $ putStrLn "No more videos"
    contPlay (a:as) = do
      r <- lift $ do
        let player = maybe "mpv" T.unpack $ conf ^. cPlayer
        void $ rawSystem player [T.unpack a]
        let
          textPair (k, v) = T.unpack $ k <> ": " <> v
          dumpDirsHelp
            = conf ^.. cDumpDirs
            . to M.toAscList
            . traversed
            . to textPair
          help = [ "what to do next ?"
                 , "q: Quit"
                 ] ++ dumpDirsHelp ++
                 [ "any key - continue" ]
        mapM_ putStrLn help
        getLine

      case L.map toLower r of
        "q" -> do
          modify $ H.adjust (+1) a
          return ()
        x -> case conf ^? cDumpDirs . ix (T.pack x) of
          Nothing -> do
            lift $ putStrLn "Unknown command: continue playing"
            modify $ H.adjust (+1) a
            contPlay as
          Just (T.unpack -> dir) -> do
            dest <- lift $ do
              putStrLn $ "Moving to: " ++ dir
              moveFile dir (T.unpack a)
            modify $ \m ->
              let oldK = fromMaybe 0 $ H.lookup a m
              in H.insert (T.pack dest) (oldK + 1) $ H.delete a m
            contPlay as

shuffleMap :: DBFiles -> IO [T.Text]
shuffleMap hm = do
  putStrLn "Shuffling ..."
  g <- newStdGen
  ret <- evalRandT rnd g
  return $ L.map fst $ concat ret
  where
    ls = groupBy ((==) `on` snd) $ sortBy (compare `on` snd) $ H.toList hm
    rnd = mapM shuffleM ls

moveFile
  :: FilePath
  -- ^ Directory to move file in
  -> FilePath
  -- ^ Full file path
  -> IO FilePath
moveFile dirPath fromName = do
  rel <- makeRelativeToCurrentDirectory fromName
  let rmDir = dirPath </> (takeDirectory rel)
  createDirectoryIfMissing True rmDir
  let toName = rmDir </> (takeFileName fromName)
  renameFile fromName toName
  return toName
