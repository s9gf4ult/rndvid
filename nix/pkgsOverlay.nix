self: super:
let haskellOverlay = import ./haskellOverlay { haskellLib = super.haskell.lib; };
in {
  inherit (self.haskellPackages.extend haskellOverlay) rndvid ;
}
