{ haskellLib }: self: super:

with haskellLib; {
  rndvid = super.callPackage ../../rndvid {} ;
  FilePather = super.callPackage ./FilePather.nix {} ;
}
